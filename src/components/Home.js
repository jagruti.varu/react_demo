import React, {Component} from 'react';
import Product from './Product'
import {Link} from 'react-router-dom';
import "../Home.css";
import 'bootstrap/dist/css/bootstrap.min.css';

export default class Home extends Component {


    constructor(props) {
        super(props);

        this.state = {
            products: []
        }
    }

    componentDidMount() {
        var data = require('../products'); // forward slashes will depend on the file location

        let products = [];

        data.forEach((product) => {
            products.push(product);
        });

        this.setState({
            products
        });
    }

    render(props) {
        // const containerStyle = {
        //     display: "flex",
        //
        // };
        const divStyle = {
            width: "20rem"
        };
        return <div className="container" >
            <div>
                <h1 className="text-secondary text-uppercase pb-md-0">Products</h1>
            </div>
            <div className="Container">
            <div className="row">
            {this.state.products.map((product) =>
                <div key={product.id} >
                        <div className="card" style={divStyle}>
                            <img className="card-img-top" src={product.image} alt="Card image cap"/>
                            <div className="card-body">
                                <h5 className="card-title">{product.name}</h5>
                                <p className="card-text">{product.detail}</p>
                                <Link key={product.id} className="btn btn-primary"
                                      to={'/product/' + product.id}>Details</Link>
                            </div>
                        </div>
                </div>
            )}
            </div>
            </div>
        </div>;
    }
}