import React, { Component } from 'react';
// import { Link } from 'react-router-dom';

export default class Product extends Component {

    constructor(props) {
        super(props);

        this.state = {
            product: null
        }
    }

    componentDidMount() {
        var data = require('../products'); // forward slashes will depend on the file location

        let products = [];

        data.forEach((product) => {
            products.push(product);
        });


        this.setState({
            products
        });
    }

    render() {
        var data = require('../products'); // forward slashes will depend on the file location

        let product = null;

        console.log(this.props);
        data.forEach((productItem) => {
            productItem.id == this.props.match.params.id ? product = productItem : null
        });
        const divStyle = {
          height:"800px",
            width:"100%"

        };

        return (
            <div className="container">
                <div className="card">
                    <div className="card-header"><b>{product.name}</b></div>
                <img className="card-img-top" src={product.image} alt="Card image cap" style={divStyle}/>
                <div className="card-body">
                    <h5 className="card-title">{product.name}</h5>
                    <p className="card-text"><b>Price:</b>{product.price}</p>
                    <p className="card-text"><b>Detail:</b>{product.detail}</p>
                    <p className="card-text"><b>Information:</b>{product.info}</p>
               </div>
                </div>
             </div>
        )
    }
}