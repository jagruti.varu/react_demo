import React, { Component } from 'react';
import './App.css';
import Product from './components/Product';
import Home from './components/Home';
import 'bootstrap/dist/css/bootstrap.min.css';


import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

class App extends Component {

  render() {
    return (
        <Router>
            <div>
                <Route exact path="/" component={Home} />
                <Route path="/product/:id" component={Product} />
            </div>
        </Router>

    );
  }
}

export default App;
